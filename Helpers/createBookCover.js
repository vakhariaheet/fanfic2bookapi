const { createCanvas, ImageData, registerFont } = require('canvas');
module.exports = (bookInfo) => {
	// Create a pattern, offscreen
	const patternCanvas = createCanvas(50, 50);
	const patternContext = patternCanvas.getContext('2d');

	// Give the pattern a background color and draw an arc
	// patternContext.fillStyle = '#55C1FF';
	patternContext.fillStyle = '#715AFF';
	patternContext.fillRect(0, 0, patternCanvas.width, patternCanvas.height);
	patternContext.arc(0, 0, 50, 0, 0.5 * Math.PI);
	patternContext.stroke();
	const canvas = createCanvas(270, 360);
	const ctx = canvas.getContext('2d');
	ctx.textAlign = 'center';
	registerFont('./Merienda-Regular.ttf', { family: 'Merienda' });
	const pattern = ctx.createPattern(patternCanvas, 'repeat');
	ctx.fillStyle = pattern;
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	ctx.globalCompositeOperation = 'destination-out';
	ctx.font = '30px Merienda';
	const wordsArray = bookInfo.title.split(' ');
	let words = [];
	const wordLen = wordsArray.length;
	for (let i = 0; i < wordLen; i++) {
		words.push(wordsArray.splice(0, 2).join(' '));
	}

	words = words.filter((word) => word);
	let height = 80;
	words.forEach((word) => {
		ctx.fillText(word, canvas.width / 2, height,260);
		height += 40;
	});
	ctx.fillText(`by ${bookInfo.author}`, canvas.width / 2, canvas.height - 20,260);
	return canvas.toDataURL();
};
