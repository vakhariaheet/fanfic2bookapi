const fs = require('fs');
const cloudinary = require('cloudinary').v2;

cloudinary.config({
	cloud_name: 'xdidhn1mvm',
	api_key: '254927581387386',
	api_secret: 'xzCKJMVvPTUT-OFbb_vRPhnINZQ',
});
module.exports = async (bookData, book) => {
	let file =
		`module.export={` +
		Object.keys(bookData)
			.map((key) => {
				return `${key}:\`${bookData[key]}\`,`;
			})
			.join('') +
		`book:[` +
		book
			.map((v) => {
				return `{title: \`${v.title}\`,data:\`${v.data}\`}`;
			})
			.join(',') +
		`]}`;
	await fs.writeFileSync(`${bookData.uid}.js`, file);
	const buffer = await fs.readFileSync(`${bookData.uid}.js`);
	cloudinary.uploader
		.upload_stream(
			{
				resource_type: 'raw',
				folder: 'bookData',
				format: 'js',
				public_id: `${bookData.uid}.js`,
			},
			(err, result) => {
				if (err) {
					return console.log(err);
				}
				fs.rm(`${bookData.uid}.js`, (err) => {
					if (err) {
						return console.log(err);
					}
				});


			},
		)
		.end(buffer);
};
